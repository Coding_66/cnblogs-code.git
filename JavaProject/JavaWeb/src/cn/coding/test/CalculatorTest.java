package cn.coding.test;

import cn.coding.junit.Calculator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class CalculatorTest {
    @Before
    public void testInit(){
        System.out.println("我被初始化了");
    }
    @After
    public void testClose(){
        System.out.println("我要走了");
    }


    public void testAdd(){

        Calculator c = new Calculator();
        int result = c.add(3, 5);
        Assert.assertEquals(8, result);
        System.out.println("相加的结果是："+result);
    }
    @Test
    public void testSub(){
        Calculator c = new Calculator();
        int result = c.sub(3, 5);
        Assert.assertEquals(-2, result);
    }

}
